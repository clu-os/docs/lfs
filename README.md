
Read the latest [CI/CD](https://en.wikipedia.org/wiki/CI/CD) version online:

- [HTML (separate pages)](https://clu-os.gitlab.io/docs/lfs/)
- [HTML (single page)](https://clu-os.gitlab.io/docs/lfs/LFS-BOOK.xhtml)
- PDF

Differences in this fork:

- [x] [Link relationships](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/rel) have been re-enabled (`prev`, `next`, `up`, `home`, `license`)
- [x] [Open Graph](https://ogp.me/) metadata has been added, social timeline cards support (`<meta property="og:*">`)
- [x] Support for configurable filename extension has been re-enabled ([html.ext](https://docbook.sourceforge.net/release/xsl/current/doc/html/html.ext.html))
- [ ] Support for configurable output character encoding ([chunker.output.encoding](https://docbook.sourceforge.net/release/xsl/current/doc/html/chunker.output.encoding.html))
- [x] Some filenames are based on element `id` ([use.id.as.filename](https://docbook.sourceforge.net/release/xsl/current/doc/html/use.id.as.filename.html))
- [ ] Parallel `make`, `WIP`
- [ ] Incremental `make`, `WIP`
- [x] Same `make` targets for all books (`html`, `pdf`, `nochunks`, ...)
- [x] CSS stylesheets have been combined, for all media (`screen` and `print`)
- [ ] "Dark Mode", `TODO`

Visit the official, upstream, [Linux From Scratch homepage](https://www.linuxfromscratch.org/lfs/)

Original (almost) README below:
___

LFS Book README
===============

This document is meant to instruct the user on how to convert the book's XML
source to other formats (e.g. HTML, PDF, PS and TXT).  First, if you have not
already done so, please read the [`INSTALL.md`](INSTALL.md) file for instructions on how to install
the required software.  If you have already completed the steps outlined in the
[`INSTALL.md`](INSTALL.md) file, then continue reading for examples how to convert these files into
various other formats.

In all examples, setting the parameter `REV=systemd` is needed to build the
`systemd` version of the book.

XML to XHTML:
-------------

```sh
make BASEDIR=/path/to/output/location
```

XML to single file XHTML (`nochunks`):
--------------------------------------

```sh
make BASEDIR=/path/to/output/location nochunks
```

XML to TXT:
-----------

Follow the instructions for `nochunks` and then run:

```sh
lynx -dump /path/to/nochunks >/path/to/output
```

XML to PDF:
-----------

```sh
make BASEDIR=/path/to/output/location pdf
```

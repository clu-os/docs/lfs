RENDERTMP   ?= $(HOME)/tmp
CHUNK_QUIET  = 1
ROOT_ID      =
HTML_EXT    ?= html
SHELL        = /bin/bash

ifdef V
	Q =
else
	Q = @
endif

ifndef REV
	REV = sysv
endif

ifneq ($(REV), sysv)
	ifneq ($(REV), systemd)
		$(error REV must be 'sysv' (default) or 'systemd'.)
	endif
endif

ifeq ($(REV), sysv)
	BASEDIR          ?= $(HOME)/public_html/lfs-book
	DUMPDIR          ?= $(HOME)/lfs-commands
	BASE_COMBINED    ?= sysv-base-combined.xml
	BASE_PROFILE     ?= sysv-base-profile.xml
	HTML_PROFILE     ?= sysv-html-profile.xml
	PDF_PROFILE      ?= sysv-pdf-profile.xml
	PDF_FO           ?= sysv-pdf.fo
	NOCHUNKS_OUTPUT  ?= LFS-BOOK.$(HTML_EXT)
	PDF_OUTPUT       ?= LFS-BOOK.pdf
else
	BASEDIR          ?= $(HOME)/public_html/lfs-systemd
	DUMPDIR          ?= $(HOME)/lfs-sysd-commands
	BASE_COMBINED    ?= systemd-base-combined.xml
	BASE_PROFILE     ?= systemd-base-profile.xml
	HTML_PROFILE     ?= systemd-html-profile.xml
	PDF_PROFILE      ?= systemd-pdf-profile.xml
	PDF_FO           ?= systemd-pdf.fo
	NOCHUNKS_OUTPUT  ?= LFS-SYSD-BOOK.$(HTML_EXT)
	PDF_OUTPUT       ?= LFS-SYSD-BOOK.pdf
endif

$(RENDERTMP) $(BASEDIR) $(DUMPDIR):
	$(Q)mkdir -p $@

.PHONY: all html nochunks pdf clean validate profile-html profile-pdf wget-list md5sums version dump-commands dist

all: html nochunks pdf dump-commands

html: $(BASEDIR)/index.html $(BASEDIR)/wget-list $(BASEDIR)/wget-list-$(REV) $(BASEDIR)/md5sums
$(BASEDIR)/index.html: stylesheets/lfs-chunked.xsl $(RENDERTMP)/$(HTML_PROFILE) obfuscate.sh | $(BASEDIR)

#	NOTE: Replace static filenames defined by <?dbhtml?>
#	NOTE: <?dbhtml filename?> directives, and this sed patch, can be removed if
#	      `id`s are defined and chunk option `use.id.as.filename` is set
	$(Q)sed \
		-e '/dbhtml filename/s/.html"/.'"$(HTML_EXT)"'"/' \
		$(RENDERTMP)/$(HTML_PROFILE) \
		> $(RENDERTMP)/$(HTML_PROFILE)-ext

	@echo "Generating chunked XHTML files at $(BASEDIR)/ ..."
	$(Q)xsltproc \
		--nonet \
		--stringparam chunk.quietly $(CHUNK_QUIET) \
		--stringparam rootid "$(ROOT_ID)" \
		--stringparam base.dir $(BASEDIR)/ \
		--stringparam html.ext ".$(HTML_EXT)" \
		stylesheets/lfs-chunked.xsl \
		$(RENDERTMP)/$(HTML_PROFILE)-ext

	@echo "Copying CSS code and images ..."
	$(Q)mkdir -p $(BASEDIR)/stylesheets
	$(Q)cp stylesheets/lfs-xsl/*.css $(BASEDIR)/stylesheets
	$(Q)cp -R images $(BASEDIR)/

	@echo "Running Tidy and obfuscate.sh on chunked XHTML ..."
#	BUG: sed should not have to fix MIME type
	$(Q)for filename in `find $(BASEDIR) -name "*.$(HTML_EXT)"`; \
	do \
		tidy -config tidy.conf $$filename || test $$? -le 1; \
		bash obfuscate.sh $$filename; \
		sed -i \
			-e '1,20s:text/html:application/xhtml+xml:g' \
			$$filename; \
	done

	@echo "Chunked XHTML created at $(BASEDIR)/"

nochunks: $(BASEDIR)/$(NOCHUNKS_OUTPUT) $(BASEDIR)/wget-list $(BASEDIR)/wget-list-$(REV) $(BASEDIR)/md5sums
$(BASEDIR)/$(NOCHUNKS_OUTPUT): stylesheets/lfs-nochunks.xsl $(RENDERTMP)/$(HTML_PROFILE) obfuscate.sh | $(BASEDIR)
	@echo "Generating non-chunked XHTML file at $(BASEDIR)/ ..."
	$(Q)xsltproc \
		--nonet \
		--stringparam rootid "$(ROOT_ID)" \
		--output $(BASEDIR)/$(NOCHUNKS_OUTPUT) \
		stylesheets/lfs-nochunks.xsl \
		$(RENDERTMP)/$(HTML_PROFILE)

	@echo "Running Tidy on non-chunked XHTML ..."
	$(Q)tidy -config tidy.conf $(BASEDIR)/$(NOCHUNKS_OUTPUT) || test $$? -le 1

	@echo "Running obfuscate.sh on non-chunked XHTML ..."
	$(Q)bash obfuscate.sh $(BASEDIR)/$(NOCHUNKS_OUTPUT)

#	BUG: sed should not have to fix MIME type
	$(Q)sed -i \
		-e '1,20s:text/html:application/xhtml+xml:g' \
		-e 's:../wget-list:wget-list:' \
		-e 's:../md5sums:md5sums:' \
		$(BASEDIR)/$(NOCHUNKS_OUTPUT)

	@echo "$(BASEDIR)/$(NOCHUNKS_OUTPUT) created"

pdf: $(BASEDIR)/$(PDF_OUTPUT) $(BASEDIR)/wget-list $(BASEDIR)/wget-list-$(REV) $(BASEDIR)/md5sums
$(BASEDIR)/$(PDF_OUTPUT): stylesheets/lfs-pdf.xsl $(RENDERTMP)/$(PDF_PROFILE) pdf-fixups.sh | $(BASEDIR)
	@echo "Generating FO file ..."
	$(Q)xsltproc \
		--nonet \
		--stringparam rootid "$(ROOT_ID)" \
		--output $(RENDERTMP)/$(PDF_FO) \
		stylesheets/lfs-pdf.xsl \
		$(RENDERTMP)/$(PDF_PROFILE)

	$(Q)sed -i \
		-e 's/span="inherit"/span="all"/' \
		$(RENDERTMP)/$(PDF_FO)

	$(Q)bash pdf-fixups.sh $(RENDERTMP)/$(PDF_FO)

	@echo "Generating PDF file ..."
	$(Q)cp -R images $(RENDERTMP)/

	$(Q)fop -q $(RENDERTMP)/$(PDF_FO) $(BASEDIR)/$(PDF_OUTPUT) 2>fop.log

	@echo "$(BASEDIR)/$(PDF_OUTPUT) created"
	@echo "fop.log created"

clean:
	@echo "Cleaning version files"
	$(Q)rm -f \
		conditional.ent \
		version.ent \
		lfs-bootscripts-*.tar.xz

	@echo "Cleaning $(BASEDIR)"
	$(Q)rm -rf $(BASEDIR)/*

	@echo "Cleaning $(RENDERTMP)"
	$(Q)rm -rf $(RENDERTMP)/*

	@echo "Cleaning $(DUMPDIR)"
	$(Q)rm -rf $(DUMPDIR)/*

validate: $(RENDERTMP)/$(BASE_PROFILE)
$(RENDERTMP)/$(BASE_PROFILE): stylesheets/lfs-xsl/profile.xsl general.ent packages.ent patches.ent version | $(RENDERTMP)
#	TODO: Integrate into Makefile
	@echo "Processing bootscripts ..."
	$(Q)bash process-scripts.sh

	@echo "Adjusting for revision $(REV) ..."
	$(Q)xsltproc \
		--nonet \
		--xinclude \
		--stringparam profile.revision $(REV) \
		--output $(RENDERTMP)/$(BASE_COMBINED) \
		stylesheets/lfs-xsl/profile.xsl \
		index.xml

	@echo "Validating the book ..."
	$(Q)xmllint \
		--nonet \
		--encode UTF-8 \
		--postvalid \
		--output $(RENDERTMP)/$(BASE_PROFILE) \
		$(RENDERTMP)/$(BASE_COMBINED)

#	TODO: Add to clean target instead of here
	@echo "Removing bootscripts ..."
	$(Q)rm -f appendices/*.script

#	TODO: Integrate into Makefile
	$(Q)./aux-file-data.sh $(RENDERTMP)/$(BASE_PROFILE)

	@echo "Validation complete."

profile-html: $(RENDERTMP)/$(HTML_PROFILE)
$(RENDERTMP)/$(HTML_PROFILE): stylesheets/lfs-xsl/profile.xsl $(RENDERTMP)/$(BASE_PROFILE)
	@echo "Generating profiled XML for XHTML ..."
	$(Q)xsltproc \
		--nonet \
		--stringparam profile.condition "$(HTML_EXT)" \
		--output $(RENDERTMP)/$(HTML_PROFILE) \
		stylesheets/lfs-xsl/profile.xsl \
		$(RENDERTMP)/$(BASE_PROFILE)

profile-pdf: $(RENDERTMP)/$(PDF_PROFILE)
$(RENDERTMP)/$(PDF_PROFILE): stylesheets/lfs-xsl/profile.xsl $(RENDERTMP)/$(BASE_PROFILE)
	@echo "Generating profiled XML for PDF ..."
	$(Q)xsltproc \
		--nonet \
		--stringparam profile.condition pdf \
		--output $(RENDERTMP)/$(PDF_PROFILE) \
		stylesheets/lfs-xsl/profile.xsl \
		$(RENDERTMP)/$(BASE_PROFILE)

DOWNLOADS_DEP = chapter03/packages.xml chapter03/patches.xml \
                packages.ent patches.ent general.ent

wget-list: $(BASEDIR)/wget-list $(BASEDIR)/wget-list-$(REV)
$(BASEDIR)/wget-list: stylesheets/wget-list.xsl $(DOWNLOADS_DEP) version | $(BASEDIR)
	@echo "Generating consolidated wget list at $(BASEDIR)/wget-list ..."
	$(Q)mkdir -p $(BASEDIR)
	$(Q)xsltproc \
		--nonet \
		--xinclude \
		--output $(BASEDIR)/wget-list \
		stylesheets/wget-list.xsl \
		chapter03/chapter03.xml

$(RENDERTMP)/files-list-$(REV).xml: stylesheets/lfs-xsl/profile.xsl $(DOWNLOADS_DEP) version | $(RENDERTMP)
	$(Q)xsltproc \
		--nonet \
		--xinclude \
		--stringparam profile.revision $(REV) \
		--output $(RENDERTMP)/files-list-$(REV).xml \
		stylesheets/lfs-xsl/profile.xsl \
		chapter03/chapter03.xml

$(BASEDIR)/wget-list-$(REV): stylesheets/wget-list.xsl $(RENDERTMP)/files-list-$(REV).xml | $(BASEDIR)
	$(Q)xsltproc \
		--nonet \
		--output $(BASEDIR)/wget-list-$(REV) \
		stylesheets/wget-list.xsl \
		$(RENDERTMP)/files-list-$(REV).xml

md5sums: $(BASEDIR)/md5sums
$(BASEDIR)/md5sums: stylesheets/wget-list.xsl $(RENDERTMP)/files-list-$(REV).xml | $(BASEDIR)
	@echo "Generating consolidated md5sum file at $(BASEDIR)/md5sums ..."
	$(Q)xsltproc \
		--nonet \
		--output $(BASEDIR)/md5sums \
		stylesheets/md5sum.xsl \
		$(RENDERTMP)/files-list-$(REV).xml

	$(Q)sed -i \
		-e "s/BOOTSCRIPTS-MD5SUM/$(shell md5sum lfs-bootscripts*.tar.xz | cut -d' ' -f1)/" \
		$(BASEDIR)/md5sums

version:
	@echo "Generating conditional.ent and version.ent XML entities ..."
	$(Q)./git-version.sh $(REV)

dump-commands: stylesheets/dump-commands.xsl $(RENDERTMP)/$(BASE_PROFILE) | $(DUMPDIR)
	@echo "Dumping book commands at $(DUMPDIR)/ ..."
	$(Q)xsltproc \
		--nonet \
		--output $(DUMPDIR)/ \
		stylesheets/dump-commands.xsl \
		$(RENDERTMP)/$(BASE_PROFILE)

	@echo "Dumping book commands complete at $(DUMPDIR)/"

dist:
	$(Q)DIST=/tmp/LFS-RELEASE ./git-version.sh $(REV)
	$(Q)rm -f lfs-$$(</tmp/LFS-RELEASE).tar.xz
	$(Q)tar cJf lfs-$$(</tmp/LFS-RELEASE).tar.xz \
		$(shell git ls-tree HEAD . --name-only -r) version.ent \
		-C /tmp LFS-RELEASE \
		--transform "s,^,lfs-$$(</tmp/LFS-RELEASE)/,"

	$(Q)echo "Generated XML tarball lfs-$$(</tmp/LFS-RELEASE).tar.xz"

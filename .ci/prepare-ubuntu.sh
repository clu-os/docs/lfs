#! /bin/sh

# Last tested with container image: ubuntu:22.04

# Add DEBIAN_FRONTEND=noninteractive to the container environment!

# Configure a UTF-8 locale to be generated on locales install
echo "${LANG} UTF-8" >> /etc/locale.gen

# Install necessary packages
apt-get update
apt-get --assume-yes install --no-install-recommends \
	locales \
	xz-utils \
	perl git make xsltproc docbook-xml docbook-xsl libxml2-utils tidy
